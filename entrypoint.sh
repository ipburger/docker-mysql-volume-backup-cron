#!/usr/bin/env bash

set -e

[ "$DEBUG" == 'true' ] && set -x

DAEMON=crond

stop() {
    echo "Received SIGINT or SIGTERM. Shutting down $DAEMON"
    pid=$(cat /tmp/$DAEMON.pid)
    kill -SIGTERM "${pid}"
    wait "${pid}"
    echo "Done."
}

echo "#------------ START entrypoint MODIFICATIONS ------------#" >> /etc/crontabs/root
for V in ${!CRON_D_*}
do
  echo "${!V}" >> /etc/crontabs/root
done
echo "#------------ END entrypoint MODIFICATIONS ------------#" >> /etc/crontabs/root


echo "Running $@"
if [[ "$(basename $1)" = *"$DAEMON"* ]]; then
    trap stop SIGINT SIGTERM
    $@ &
    pid="$!"
    echo "${pid}" > /tmp/$DAEMON.pid
    wait "${pid}" && exit $?
else
    exec "$@"
fi
