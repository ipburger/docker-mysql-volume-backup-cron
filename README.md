# This Image

A cron job runs every 8 hours.  Override this by setting cron job spec's in
the `CRON_D_BACKUP` environment variable.

It backups all databases, unless `DBS`
is specified as a space separated list of DB's to backup, using `mysqldump`.

By default, `MYSQLDUMP_OPTIONS` is assigned to `--single-transaction=true`. You can overwrite the option by specifying the variable.

The file are `gzip`ed. The optional `PREFIX` can be used for adding the prefix to the backup file name.

If the DB is linked to this container make sure that `MYSQL_ROOT_PASSWORD` is set in the linked mysql container.

You can use docker network to connect to the DB container with `--net` as well. If you go this way, make sure that both `MYSQL_ROOT_PASSWORD` and `MYSQL_HOST`(container name) are set in this backup container.

You can choose 2 types of backup destination, using $STORAGE_TYPE env var: `swift` for OpenStack Object Storage and `local` for local file system.

You can backup volumes as well, mounted on this backup container on any path that are specified by environment variables starting
with `VOLUME_`. For example:

Mounted volumes:
backup on `/volumes/backup`
info on `/volumes/info`

Also add these env vars on the container:
`VOLUME_BACKUP="/volumes/backup"`
`VOLUME_INFO="/volumes/info"`

## OpenStack Swift (`swift`)

The following environment variables must be provided.

- `OS_TENANT_NAME`
- `OS_USERNAME`
- `OS_PASSWORD`
- `OS_AUTH_URL`
- `CONTAINER`

## Local File System (`local`)

The following environment variable must be provided.

- `BACKUP_DIR`

# Build

    docker build -t mysql-backup-cron .

# Run

Schedule UTC 1:00 am, 9:00 am and 5:00 pm per day.

```
    docker run -tid --name mysql-backup-cron \
        --net my_network_name \
        -e MYSQL_ROOT_PASSWORD=my_root_password \
        -e MYSQL_HOST=mysql_host \
        -e BACKUP_DIR=/backup \
        -e PREFIX=subdir/here/with-prefix \
        -v $(pwd):/backup mysql-backup-cron
```

Schedule every 5 minutes.

```
    docker run -tid --name mysql-backup-cron \
        --net my_network_name \
        -e MYSQL_ROOT_PASSWORD=my_root_password \
        -e MYSQL_HOST=mysql_host \
        -e BACKUP_DIR=/backup \
        -e CRON_D_BACKUP="*/5 * * * * root /backup.sh | logger" \
        -e PREFIX=subdir/here/with-prefix \
        -v $(pwd):/backup mysql-backup-cron
```

# Exec

Use `docker exec <container> /backup.sh` to take an immediate backup.

Use `docker exec <container> /restore.sh` to list available backups to restore
from. Then `docker exec /restore.sh <filename of backup>` to
restore it. You can also run `docker exec /restore.sh __latest__` for restoring from the latest backup file.

# Backup

## File Name format

```
  2017-01-20-000252.sql.gz
  `--+`-+`-+`---+--`---+--
     |  |  |    |      +------- suffix
     |  |  |    +-------------- time
     |  |  +------------------- day
     |  +---------------------- month
     +------------------------- year
```

## Backup Cleanup Schedule and Behavior

Specify how many backups are to be kept with 'MAX_BACKUP_FILES' env var. Then set up the `delete.sh`
script to be run with the `CRON_D_*` var, eg.

Clean up everyday at 12h if there are more then 30 backups:

Pass to the container these vars
MAX_BACKUP_FILES=`30`
CRON_D_CLEAN_BACKUPS=`00 12 * * * bash /delete.sh`
