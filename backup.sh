#!/bin/bash

# Generate a (gzipped) dumpfile for each database specified in ${DBS}.
# Upload to the given type of storage

# Bailout if any command fails
set -e

[ "$DEBUG" == 'true' ] && set -x

#Make logs cleaner
echo "---------------------------------------------\n"

# Create a temporary directory to hold the backup files.
find /tmp -type d | grep -v "^/tmp$" | xargs rm -fr
DIR=$(mktemp -d)

# Generate a timestamp to name the backup files with.
TS=$(date +%Y-%m-%d-%H%M%S)

#################################
#      BACKING UP MYSQL 
#################################

if [ -z "${SKIP_MYSQL-}" ]; then
  # Specify mysql host (mysql by default)
  MYSQL_HOST=${MYSQL_HOST:-mysql}
  MYSQL_ROOT_PASSWORD=${MYSQL_ENV_MYSQL_ROOT_PASSWORD:-${MYSQL_ROOT_PASSWORD}}
  MYSQLDUMP_OPTIONS=${MYSQLDUMP_OPTIONS:-"--single-transaction=true"}

  ############################################
  #  CREATE BACKUP FILE(S) OF THE DATABASE(S)
  ############################################
  # Backup all databases, unless a list of databases has been specified
  BASE_DIR=`dirname ${DIR}/${PREFIX}test`
  if [ ! -d "$BASE_DIR" ]; then
    mkdir -p $BASE_DIR
  fi
  if [ -z "$DBS" ]
  then
    # Backup all DB's in bulk
    mysqldump -uroot -p$MYSQL_ROOT_PASSWORD -h$MYSQL_HOST --add-drop-database --events --all-databases $MYSQLDUMP_OPTIONS | gzip > $DIR/${PREFIX}all-databases-$TS.sql.gz
  else
    # Backup each DB separately
    for DB in $DBS
    do
      mysqldump -uroot -p$MYSQL_ROOT_PASSWORD -h$MYSQL_HOST --add-drop-database -B $DB $MYSQLDUMP_OPTIONS | gzip > $DIR/$PREFIX$DB-$TS.sql.gz
    done
  fi
fi

#################################
#      BACKING UP VOLUMES
#################################
for V in ${!VOLUME_*}
do
  tar -zcf $DIR/${PREFIX}volume-${V#VOLUME_}-$TS.tar.gz ${!V}
done

#################################
#      BACKING UP MONGO
#################################
if [[ -v MONGO_HOST_AND_PORT ]]; then
  mongodump --host="${MONGO_HOST_AND_PORT}" --username="${MONGO_USERNAME}" --password="${MONGO_PASSWORD}" --db="${MONGO_DB}" --archive="${DIR}/${PREFIX}mongo-backup-${TS}.tar.gz" --gzip
fi

#################################
#        UPLOAD BACKUPS
#################################

case $STORAGE_TYPE in
  swift)
    # Upload the backups to Swift
    cd $DIR
    for f in `find . -type f \( -name "*.sql.gz" -o -name "*.tar.gz" \)`
    do
      # Avoid Authorization Failure error
      #TODO: Change this $CONTAINER var to $OS_CONTAINER
      swift upload $CONTAINER ${f}
    done
    ;;
  local)
    # cp the backup files in the temp directory to the backup directory
    cd $DIR
    for f in `find . -type f \( -name "*.sql.gz" -o -name "*.tar.gz" \) | grep "^./${PREFIX}"`
    do
      BASE_DIR=`dirname ${BACKUP_DIR}/${PREFIX}test`
      if [ ! -d "${BASE_DIR}" ]; then
        mkdir -p ${BASE_DIR}
      fi
      cp -f ${f} ${BASE_DIR}
    done
    ;;
esac

# Clean up
rm -rf $DIR

#Make logs cleaner
echo "---------------------------------------------\n\n"
