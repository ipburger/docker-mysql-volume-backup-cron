#!/bin/bash

. /_list.sh

# Bailout if any command fails
set -e

[ "$DEBUG" == 'true' ] && set -x

if [ -z "${MAX_BACKUP_FILES+x}" ]; then
  echo "Can't delete backups if MAX_BACKUP_FILES is not defined as a number"
  exit 1
fi

function delete {
  case $STORAGE_TYPE in
    s3)
      s3cmd --access_key=$ACCESS_KEY --secret_key=$SECRET_KEY --region=$REGION del $BUCKET/$1
      ;;
    swift)
      swift delete $CONTAINER $1
      ;;
    gcs)
      gsutil rm -fr gs://$GC_BUCKET/$1
      ;;
    local)
      rm -f ${BACKUP_DIR}/$1
      ;;
  esac
}

list_backup_files

if [ -n "${ALL_BACKUP_FILES}" ]; then
  echo "------------------------"
  echo "Starting backup cleaner"

  NUM_FILES=$(echo "${ALL_BACKUP_FILES}" | wc -l)
  DELETED_FILES=0
  for f in ${ALL_BACKUP_FILES}
  do
    let REMAINING_FILES=NUM_FILES-DELETED_FILES
    if [[ "${MAX_BACKUP_FILES}" -le "${REMAINING_FILES}" ]]; then
      delete ${f}
      let DELETED_FILES=DELETED_FILES+1
    fi
  done
  echo "Cleaner finished"
  echo "------------------------"
fi
